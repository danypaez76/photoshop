#!/usr/bin/env python

#Se importa de flask a Flask, jsonify y json

from flask import Flask,jsonify,json

#Se importa empleados import Empleado

from empleado import Empleado

#Se importa names

import names

#Se importa sys

import sys

lista1= {apellido :'Paez', nombre:'daniel' }

#Se crea la instancia de la aplicacion

app = Flask(__name__)



#Se crea

#Se define el decorador route donde se da la ruta del servidor web.

@app.route("/ListaEmpleados")

def ListaEmpleados():

    try:

        #inicializar la lista de empleados

        listaEmpleados = []

        #crear instancias para llenar la lista

        for i in range(0,20):

            empleado = Empleado(names.get_first_name(),names.get_last_name())

            listaEmpleados.append(empleado)



        # convertir en dato json al diccionario

        jsonStr = json.dumps([e.toJSON() for e in listaEmpleados])

    except :

        print ("error", sys.exc_info()[0])



    #Retorna el json

    return jsonStr

@app.route("/lista2")
def listar(): 
    return (data=lista2)



if __name__ == '__main__':

    app.run(debug=True)